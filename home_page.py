from result_page import ResultPage


class HomePage(object):
    def __init__(self, driver):
        self.driver = driver

    def search(self, search_text):
        self.driver.find_element_by_css_selector(".input__control.input__input").send_keys(search_text)
        self.driver.find_element_by_css_selector("div.search2__button").click()
        return ResultPage
