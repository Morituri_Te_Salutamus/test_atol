class ResultPage(object):

    def __init__(self, driver):
        self.driver = driver

    def first_link(self):
        site_link = self.driver.find_element_by_css_selector("li.serp-item div h2 a").get_attribute("href")
        return site_link
