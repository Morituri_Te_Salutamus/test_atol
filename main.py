import unittest
import requests
from home_page import HomePage
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager


class TestUnit(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(ChromeDriverManager().install())
        self.driver.implicitly_wait(10)
        self.driver.get("http://www.yandex.ru")

    def test_search_atol(self):
        home = HomePage(self.driver)
        result = home.search("Атол онлайн")
        assert "https://online.atol.ru/" in result.first_link(self)
        print("\nСайт Атол-онлайн присутствует в результатах")

        r = requests.get(result.first_link(self))
        assert r.status_code == 200
        print("\nСтатус код сайта АТОЛ равен", r.status_code)

    def tearDown(self):
        self.driver.close()
